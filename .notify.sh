#!/bin/bash

TEXT="Тестирование выполнено: (☉_☉) %0A%0AProject:+$CI_PROJECT_NAME%0AURL:+$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0ABranch:+$CI_COMMIT_REF_SLUG%0A
отчет доступен по ссылке: https://kneznai.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/result.html
%0A краткий отчет:"
curl -s -X POST https://api.telegram.org/$TELEGRAM_BOT_TOKEN/sendMessage -d chat_id=$TELEGRAM_ID_USER  -d text="$TEXT"


curl -F chat_id=$TELEGRAM_ID_USER -F document=@"public/result.html" https://api.telegram.org/$TELEGRAM_BOT_TOKEN/sendDocument