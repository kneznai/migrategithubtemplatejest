import {
  Airports, Users, Favorites, Mailboxlayer,
} from './services';

const apiProvider = () => ({
  auth: () => new Users(),
  favorites: () => new Favorites(),
  airports: () => new Airports(),
  mailboxlayer: () => new Mailboxlayer(),
});

export { apiProvider };
