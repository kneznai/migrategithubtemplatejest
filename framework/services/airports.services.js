import fetch from 'node-fetch';
import { urls } from '../config';

const Airports = function Airports() {
  this.getAirports = async function getAirports() {
    const r = await fetch(`${urls.airport}/api/airports`, {
      method: 'get',
    });
    return r;
  };
  this.getAirportById = async function getAirport(airport) {
    const r = await fetch(`${urls.airport}/api/airports/${airport}`, {
      method: 'get',
    });
    return r;
  };
  this.getDistance = async function getDistance(route) {
    const r = await fetch(`${urls.airport}/api/airports/distance`, {
      method: 'post',
      body: JSON.stringify(route),
      headers: { 'Content-Type': 'application/json' },
    });
    return r;
  };
};

export { Airports };
