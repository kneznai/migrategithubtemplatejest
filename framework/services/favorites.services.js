import supertest from 'supertest';
import { urls } from '../config';

const Favorites = function Favorites() {
  this.getFavorites = async function getFavorites(token) {
    const r = await supertest(urls.airport)
      .get('/api/favorites')
      .set('Accept', 'application/json')
      .set('Authorization', `Token ${token}`);
    return r;
  };
  this.addToFavorites = async function addToFavorites(token, airport) {
    const r = await supertest(urls.airport)
      .post('/api/favorites')
      .set('Accept', 'application/json')
      .set('Authorization', `Token ${token}`)
      .send(airport);
    return r;
  };
  this.updAirportNote = async function updAirportNote(token, airportId, airport) {
    const r = await supertest(urls.airport)
      .patch(`/api/favorites/${airportId}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer token=${token}`)
      .send(airport);
    return r;
  };
  this.delFromFavorites = async function delFromFavorites(token, airportId) {
    const r = await supertest(urls.airport)
      .delete(`/api/favorites/${airportId}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer token=${token}`);
    return r;
  };
  this.getFavoriteById = async function getFavoriteById(token, airportId) {
    const r = await supertest(urls.airport)
      .get(`/api/favorites/${airportId}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer token=${token}`);
    return r;
  };
};

export { Favorites };
