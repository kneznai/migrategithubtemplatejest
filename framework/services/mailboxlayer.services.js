import supertest from 'supertest';
import { urls } from '../config';

const Mailboxlayer = function Mailboxlayer() {
  this.checkFormat = async function checkFormat(email, accessKey) {
    const query = {
      email,
      access_key: accessKey,
      format: 1,
    };
    const r = await supertest(urls.mailboxlayer)
      .get('/api/check')
      .set('Accept', 'application/json')
      .query(query);
    return r;
  };
};

export { Mailboxlayer };
