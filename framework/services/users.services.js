import supertest from 'supertest';
import { urls, user } from '../config';

const Users = function Users() {
  this.getToken = async function getToken() {
    const request = supertest(urls.airport);
    const r = await request.post('/api/tokens').set('Accept', 'application/json').send(user.airportgap);
    const { body } = r;
    const { token } = body;
    return token;
  };
  this.getAccessKey = function getAccessKey() {
    return user.mailboxlayer.access_key;
  };
};

export { Users };
