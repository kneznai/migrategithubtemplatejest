module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',
    'htmlreport4jest'],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  // testRegex: '(9http_controllers.spec)\\.js$',
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};
