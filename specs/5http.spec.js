import {
  beforeAll, describe, expect, test,
} from '@jest/globals';
import supertest from 'supertest';
import fetch from 'node-fetch';

describe('Отправляем http', () => {
  const uri = 'https://airportgap.dev-tester.com/';

  describe('Отправляем http через supertest', () => {
    const user = {
      email: 'testairportgapkate@gmail.com',
      password: 'testkategap',
    };
    let token;
    let favAirport;

    beforeAll(async () => {
      const request = supertest(uri);
      const r = await request.post('/api/tokens').set('Accept', 'application/json').send(user);
      token = r.body.token;
    });

    test('Запросить список аэропортов', async () => {
      const request = supertest(uri);
      const r = await request.get('/api/airports').set('Accept', 'application/json');
      expect(r.status).toEqual(200);
    });

    test('Сгенерировать токен и проверить список favorites пустой', async () => {
      const r = await supertest(uri)
        .get('/api/favorites')
        .set('Accept', 'application/json')
        .set('Authorization', `Token ${token}`);
        // make sure there is no data in favourites before I add
      expect(r.status).toEqual(200);
      expect(r.body.data).toHaveLength(0);
    });

    test('Добавить аэропорт в список favorites', async () => {
      const airport = {
        airport_id: 'RKV',
        note: 'Next destination point',
      };

      const r = await supertest(uri)
        .post('/api/favorites')
        .set('Accept', 'application/json')
        .set('Authorization', `Token ${token}`)
        .send(airport);
      expect(r.status).toEqual(201);

      const { data } = r.body;
      expect(data).toBeInstanceOf(Object);
      expect(data.attributes.airport.iata).toBe(airport.airport_id);
      expect(data.attributes.note).toBe(airport.note);

      favAirport = data.id; // save airport id in favorites list for further operations
    });

    test('Изменить note в сохраненной записи', async () => {
      const updAirport = {
        note: 'Final destination point',
      };

      const r = await supertest(uri)
        .patch(`/api/favorites/${favAirport}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer token=${token}`)
        .send(updAirport);
      expect(r.status).toEqual(200);

      const { data } = r.body;
      expect(data.attributes.note).toBe(updAirport.note);
    });

    test('Удалить аэропорт из списка favorites', async () => {
      let r = await supertest(uri)
        .delete(`/api/favorites/${favAirport}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer token=${token}`);
      expect(r.status).toEqual(204);

      r = await supertest(uri)
        .get('/api/favorites')
        .set('Accept', 'application/json')
        .set('Authorization', `Token ${token}`);
      expect(r.status).toEqual(200);
      expect(r.body.data).toHaveLength(0);
    });
  });

  describe('Отправляем http через node-fetch', () => {
    const airport1 = 'JFK';
    const airport2 = 'CDG';

    test('Запросить два аэропорта', async () => {
      let r = await fetch(`${uri}/api/airports/${airport1}`, {
        method: 'get',
      });
      expect(r.status).toEqual(200);

      r = await fetch(`${uri}/api/airports/${airport2}`, {
        method: 'get',
      });
      expect(r.status).toEqual(200);
    });

    test('Запросить дистанцию между ними', async () => {
      const route = {
        from: airport1,
        to: airport2,
      };

      const r = await fetch(`${uri}/api/airports/distance`, {
        method: 'post',
        body: JSON.stringify(route),
        headers: { 'Content-Type': 'application/json' },
      });
      expect(r.status).toEqual(200);

      const { data } = await r.json();
      expect(data.type).toBe('airport_distance');
      expect(data.id).toBe(`${airport1}-${airport2}`);
    });
  });
});
