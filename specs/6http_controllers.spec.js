import {
  beforeAll, describe, expect, test,
} from '@jest/globals';
import { Airports } from '../framework/services/airports.services';
import { Users } from '../framework/services/users.services';
import { Favorites } from '../framework/services/favorites.services';

describe('Отправляем http', () => {
  describe('Отправляем http через supertest', () => {
    let token;
    let favAirport;

    beforeAll(async () => {
      token = await new Users().getToken();
    });

    test('Проверить список favorites пустой', async () => {
      const r = await new Favorites().getFavorites(token);
      // make sure there is no data in favourites before I add
      expect(r.status).toEqual(200);
      expect(r.body.data).toHaveLength(0);
    });

    test('Добавить аэропорт в список favorites', async () => {
      const airport = {
        airport_id: 'RKV',
        note: 'Next destination point',
      };

      const r = await new Favorites().addToFavorites(token, airport);
      expect(r.status).toEqual(201);

      const { data } = r.body;
      expect(data).toBeInstanceOf(Object);
      expect(data.attributes.airport.iata).toBe(airport.airport_id);
      expect(data.attributes.note).toBe(airport.note);

      favAirport = data.id; // save airport id in favorites list for further operations
    });

    test('Проверить невалидность работы защищенных методов без токена', async () => {
      const wrongToken = '=======WrOnGtOkEn=======';
      const favorites = new Favorites();

      let r = await favorites.getFavorites(wrongToken);
      expect(r.status).toEqual(401);

      const airport = {
        airport_id: 'JFK',
        note: 'Next destination point',
      };

      r = await favorites.addToFavorites(wrongToken, airport);
      expect(r.status).toEqual(401);

      const updAirport = {
        note: 'Final destination point',
      };

      r = await favorites.updAirportNote(wrongToken, favAirport, updAirport);
      expect(r.status).toEqual(401);

      r = await favorites.delFromFavorites(wrongToken, favAirport);
      expect(r.status).toEqual(401);
    });

    test('Проверить невозможность добавить один и тот же аэропорт дважды', async () => {
      const favorites = new Favorites();

      let r = await favorites.getFavoriteById(token, favAirport);
      expect(r.status).toEqual(200);

      const { data } = r.body;
      const airport = {
        airport_id: data.attributes.airport.iata,
        note: 'Next destination point -again',
      };

      r = await favorites.addToFavorites(token, airport);
      expect(r.status).toEqual(422);
    });

    test('Изменить note в сохраненной записи', async () => {
      const updAirport = {
        note: 'Final destination point',
      };

      const r = await new Favorites().updAirportNote(token, favAirport, updAirport);
      expect(r.status).toEqual(200);

      const { data } = r.body;
      expect(data.attributes.note).toBe(updAirport.note);
    });

    test('Удалить аэропорт из списка favorites', async () => {
      const favorites = new Favorites();

      let r = await favorites.delFromFavorites(token, favAirport);
      expect(r.status).toEqual(204);

      r = await favorites.getFavorites(token);
      expect(r.status).toEqual(200);
      expect(r.body.data).toHaveLength(0);
    });
  });

  describe('Отправляем http через node-fetch', () => {
    const airport1 = 'JFK';
    const airport2 = 'CDG';
    const wrongAirport = 'M01';

    test('Запросить список аэропортов', async () => {
      const r = await new Airports().getAirports();
      expect(r.status).toEqual(200);
    });

    test('Запросить два аэропорта', async () => {
      let r = await new Airports().getAirportById(airport1);
      expect(r.status).toEqual(200);

      r = await new Airports().getAirportById(airport2);
      expect(r.status).toEqual(200);
    });

    test('Запросить дистанцию между ними', async () => {
      const route = {
        from: airport1,
        to: airport2,
      };

      const r = await new Airports().getDistance(route);
      expect(r.status).toEqual(200);

      const { data } = await r.json();
      expect(data.type).toBe('airport_distance');
      expect(data.id).toBe(`${airport1}-${airport2}`);
    });

    test('Запросить несуществующий аэропорт', async () => {
      const r = await new Airports().getAirportById(wrongAirport);
      expect(r.status).toEqual(404);
    });
  });
});
