import { describe, expect, it } from '@jest/globals';
import { sumScores } from '../src/homework/homework7';

describe('Задание 7. Объекты и массивы', () => {
  it('Проверяем сумму оценок студентов группы', () => {
    const scores = { Anna: 10, Olga: 1, Ivan: 5 };
    expect(sumScores(scores)).toEqual(16);
  });

  it('Проверяем валидность оценок студентов', () => {
    const invalidScores = { Anna: undefined, Olga: 'A', Ivan: 5 };
    expect(sumScores(invalidScores)).toEqual(-1);
  });

  it('Проверяем валидность оценок студентов - неотрицательное значение', () => {
    const invalidScores = { Anna: 10, Olga: 1, Ivan: -5 };
    expect(sumScores(invalidScores)).toEqual(-1);
  });

  it('Проверяем непустой объект с оценками студентов', () => {
    const emptyScores = {};
    expect(sumScores(emptyScores)).toEqual(-1);
  });

  it('Проверяем валидность структуры данных', () => {
    let wrongScores = 'NotAnObject';
    expect(sumScores(wrongScores)).toEqual(-1);
    wrongScores = null;
    expect(sumScores(wrongScores)).toEqual(-1);
    wrongScores = undefined;
    expect(sumScores(wrongScores)).toEqual(-1);
  });
});
