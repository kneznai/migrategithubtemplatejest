import {
  beforeAll, describe, expect, test,
} from '@jest/globals';
import { apiProvider } from '../framework';
import { user } from '../framework/config';

describe('Проверка формата имейла через mailboxlayer', () => {
  let accessKey;

  beforeAll(() => {
    accessKey = apiProvider().auth().getAccessKey();
  });

  test('Проверить точно валидный email', async () => {
    const r = await apiProvider().mailboxlayer().checkFormat(user.mailboxlayer.email, accessKey);
    const { status, body } = r;
    expect(status).toEqual(200);
    expect(body).not.toHaveProperty('success');
    expect(body).toHaveProperty('format_valid');
    expect(body.format_valid).toBeTruthy();
  });

  // Проверить email формат на валидность через параметризированный тест
  test.each`
    email                               | expected  | comment
    ${'email@example.com'}              | ${true}   | ${'email is valid'}
    ${'firstname.lastname@example.com'} | ${true}   | ${'email is valid'}
    ${'email@subdomain.example.com'}    | ${true}   | ${'email is valid'}
    ${'firstname+lastname@example.com'} | ${true}   | ${'email is valid'}
    ${'email@[123.123.123.123]'}        | ${true}   | ${'email is valid'}
    ${'1234567890@example.com'}         | ${true}   | ${'email is valid'}
    ${'plainaddress'}                   | ${false}   | ${'email is invalid'}
    ${'#@%^%#$@#$@#.com'}               | ${false}   | ${'email is invalid'}
    ${'@example.com'}                   | ${false}   | ${'email is invalid'}
    ${'Joe Smith <email@example.com>'}  | ${false}   | ${'email is invalid'}
    ${'email.example.com'}              | ${false}   | ${'email is invalid'}
    `('$email is $expected email because $comment', async ({ email, expected }) => {
    const r = await apiProvider().mailboxlayer().checkFormat(email, accessKey);
    const { status, body } = r;
    expect(status).toEqual(200);
    expect(body).not.toHaveProperty('success');
    expect(body).toHaveProperty('format_valid');
    expect(body.format_valid).toEqual(expected);
  });

  test('Проверить недоступность сервиса без access_key', async () => {
    const r = await apiProvider().mailboxlayer().checkFormat(user.mailboxlayer.email, '');
    const { status, body } = r;
    expect(status).toEqual(200);
    expect(body).toHaveProperty('success');
    expect(body).toHaveProperty('error');
    expect(body.error.code).toEqual(101);
    expect(body.error.type).toEqual('missing_access_key');
  });
});
