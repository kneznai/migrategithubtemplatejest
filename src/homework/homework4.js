/**
 * kolobok
 * @param {string} name
 * @returns {string}
 */
export const kolobok = (name) => {
  let actor = '';
  let action = 'ушел';

  switch (name) {
    case 'дедушка':
      actor = 'дедушки';
      break;
    case 'бабушка':
      actor = 'бабушки';
      break;
    case 'заяц':
      actor = 'зайца';
      break;
    case 'волк':
      actor = 'волка';
      break;
    case 'медведь':
      actor = 'медведя';
      break;
    case 'лиса':
      actor = 'лисы';
      action = 'не ушел';
      break;
    default:
      break;
  }
  return `Я от ${actor} ${action}`;
};
/**
 * newYear
 * @param {string} name
 * @returns {string}
 */
export const newYear = (name) => `${name}! ${name}! ${name}!`;
