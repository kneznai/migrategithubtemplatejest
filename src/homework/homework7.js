/**
  * Эта функция суммирует оценки группы
  * sumScores
  * @param {object} scores
  * @returns {number} returns sum of scores or -1 if there is invalid or missing score
  */

export const sumScores = (scores) => {
  if (typeof scores !== 'object' || scores === null) {
    return -1;
  }

  const scoreValues = Object.values(scores);
  if (scoreValues.length === 0) {
    return -1;
  }

  const validScores = scoreValues.filter((score) => Number.isInteger(score) && score >= 0);

  if (validScores.length !== scoreValues.length) {
    return -1;
  }

  return scoreValues.reduce((total, score) => total + score, 0);
};
